### Logiciel de gestion de produits bancaires

Voir [Exigences](https://github.com/ace-lectures/H20-MGL-7460/blob/master/projets/projet-technique.md#travail-%C3%A0-effectuer "Projet Technique MGL7460-90 (Hiver 2020)")

#### Instruction pour la focntionnalité : ```./employee --add CLIENT_NAME```
#### NOTE: Malheureusement, on n'a pas réussi à faire notre script à marcher
```
git clone https://gitlab.com/presscorner/bank-products-management projet

cd projet

mvn clean install

cd bank/target

java -jar bank-0.0.1-SNAPSHOT.jar

```

#### Aller en Chrome à l'adresse : ```http://localhost:9999/h2-console```
#### Changer le JDBC URL: ```jdbc:h2:mem:testdb```
#### Click le bouton ```Connect```
#### Click la table ```CLIENTS``` et puis sur ```Run``` pour voir la liste des clients (il y a aucun)

#### Dans une autre fenetre de terminal anfiguer à la racine du projet puis :
```
cd employee/target
java -jar employee-0.0.1-SNAPSHOT.jar --add John Henry Smith
```

#### Retourner dans la console H2 et recharger la page, puis ré-exécuter la requete SQL et vous devrais voir le client ajouté.

##### Ca demontre la fonctionnalité ci-haut mentionnée. 